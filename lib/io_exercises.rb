# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  target_number = rand(1..100)
  counter = 1
  until (attempt = guessing_game_prompt) == target_number
    puts "#{attempt} is too #{attempt < target_number ? 'low' : 'high'}."
    counter += 1
  end
  puts "You got it! The number was #{target_number}."
  puts "It took you #{counter} guesses."
end

def guessing_game_prompt
  print 'Guess a number between 1 and 100: '
  gets.chomp.to_i
end

def file_shuffler
  print 'Enter a filename: '
  filename = gets.chomp
  File.open("#{filename.split('.').first}-shuffled.txt", 'w') do |file|
    file.write(File.readlines(filename).shuffle.join)
  end
end
